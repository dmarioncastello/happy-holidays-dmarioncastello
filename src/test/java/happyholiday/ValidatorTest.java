package happyholiday;

import happyholiday.formater.FormatDates;
import happyholiday.formater.FormatDestination;
import happyholiday.model.City;
import happyholiday.server.Main;
import happyholiday.validator.DateValidatorService;
import happyholiday.validator.DestinationValidatorService;
import happyholiday.validator.ProductValidatorService;
import happyholiday.validator.Validator;
import org.glassfish.grizzly.http.server.HttpServer;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;


public class ValidatorTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    private HttpServer server;
    private WebTarget target;

    @Before
    public void setUp() throws Exception {
        // start the server
        server = Main.startServer();
        // create the client
        Client c = ClientBuilder.newClient();

        // uncomment the following line if you want to enable
        // support for JSON in the client (you also have to uncomment
        // dependency on jersey-media-json module in pom.xml and Main.startServer())
        // --
        // c.configuration().enable(new org.glassfish.jersey.media.json.JsonJaxbFeature());

        target = c.target(Main.BASE_URI);
    }


    @Test
    public void checkProductIsValid(){
        Validator productValidator = new ProductValidatorService("flight");
        Assert.assertTrue(((ProductValidatorService) productValidator).checkProductExists());
    }

    @Test
    public void checkDestinationExists(){
        List<City> expectedCities = new ArrayList<>();
        expectedCities.add(new City("Manresa", "ES"));
        Validator destinationValidator = new DestinationValidatorService("Manresa",expectedCities);
        Assert.assertTrue(((DestinationValidatorService) destinationValidator).checkDestinationExists(expectedCities));
    }

    @Test
    public void checkBookingDateIsAfterLocalDate(){
        Validator dateValidator = new DateValidatorService();
        FormatDates formatDates = new FormatDates();
        Date bookingDate = formatDates.transformDateStringIntoDate("2018-07-20");
        Assert.assertTrue(((DateValidatorService) dateValidator).checkStartDateAfterActualDate(bookingDate));
    }

    @Test
    public void checkStartBookingDateisSmallerThanEndBookingDate(){
        Validator dateValidator = new DateValidatorService();
        FormatDates formatDates = new FormatDates();
        Date startDate = formatDates.transformDateStringIntoDate("2018-07-11");
        Date endDate = formatDates.transformDateStringIntoDate("2018-07-12");
        Assert.assertTrue(((DateValidatorService) dateValidator).checkStartDateBeforeThanEndDate(startDate, endDate));
    }


    @Test
    public void checkTransformDatesThrowsExceptionWhenIncorrectDateFormat() throws ParseException{
        thrown.expect(ParseException.class);
        FormatDates formatDates = new FormatDates();
        formatDates.transformDateStringIntoDate("hola");

    }


    @Test
    public void checkURI(){
        int status =  target.path("suggestions/cities").request().get().getStatus();
        assertEquals(200, status);
    }

    @Test
    public void checkURIResponseStatusIsOK(){
        int status =  target.path("suggestions/flight/madrid").queryParam("start-date", "2018-08-10").queryParam("end-date","2018-08-12").request().get().getStatus();
        int exptectedStatus =200;
        Assert.assertEquals(exptectedStatus, status);

    }

    @Test
    public void checkProductThrows422Exception(){
        int status =  target.path("suggestions/flights/madrid").queryParam("start-date", "2018-08-10").queryParam("end-date","2018-08-12").request().get().getStatus();
        int exptectedStatus =422;
        Assert.assertEquals(exptectedStatus, status);
    }

    @Test
    public void checkDestinationThrows422Exception(){
        int status =  target.path("suggestions/flight/madrids").queryParam("start-date", "2018-08-10").queryParam("end-date","2018-08-12").request().get().getStatus();
        int exptectedStatus =422;
        Assert.assertEquals(exptectedStatus, status);
    }


    @Test(expected = WebApplicationException.class)
    public void checkDestinationThorws422StatusException(){
        Validator productValidator = new ProductValidatorService("flight");
    }






}
