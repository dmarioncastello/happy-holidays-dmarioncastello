package happyholiday.formater;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

public class FormatDates {

    public FormatDates(){

    }

    public Date transformDateStringIntoDate(String startDateTxt){
        Date date = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            date =  simpleDateFormat.parse(startDateTxt);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }



    public Date getLocalDate(){
        LocalDate localDate = LocalDate.now();
        Date currentDate = null;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            currentDate = simpleDateFormat.parse(localDate.toString());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return currentDate;
    }
}
