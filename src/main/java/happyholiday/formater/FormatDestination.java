package happyholiday.formater;

import happyholiday.model.City;

import java.util.ArrayList;
import java.util.List;

public class FormatDestination {

    public FormatDestination(){

    }

    public List<City> formatCitiesToLowerCase(List<City> cities) {
        List<City> newCities = new ArrayList<>();
        for (City city : cities) {
            city.setName(city.getName().toLowerCase());
            newCities.add(city);
        }
        return newCities;
    }

    public String buildCityNames(List<City> cities) {
        StringBuilder citiesNames = new StringBuilder();
        citiesNames.append("[Following cites: ");
        for (City city : cities) {
            citiesNames.append(city.getName());
            citiesNames.append(", ");
        }
        citiesNames.append("]");
        return citiesNames.toString();
    }

}
