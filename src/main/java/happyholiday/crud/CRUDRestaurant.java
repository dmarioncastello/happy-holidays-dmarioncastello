package happyholiday.crud;

import happyholiday.model.City;
import happyholiday.model.Restaurant;
import happyholiday.service.JPAUtility;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

public class CRUDRestaurant {

    private EntityManager entityManager;

    public CRUDRestaurant() {

        this.entityManager = JPAUtility.getEntityManager();
    }

    public List<Restaurant> findAllRestaurant(City city) {
        entityManager.getTransaction().begin();
        Query query = entityManager.createNamedQuery("find restaurant by city");
        query.setMaxResults(5);

        query.setParameter("city", city);
        List<Restaurant> restaurants = query.getResultList();
        entityManager.close();
        return restaurants;
    }
}
