package happyholiday.crud;

import happyholiday.model.Accommodation;
import happyholiday.model.City;
import happyholiday.service.JPAUtility;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.Date;
import java.util.List;


public class CRUDAccommodation {

    private EntityManager entityManager;

    public CRUDAccommodation(){
        this.entityManager = JPAUtility.getEntityManager();
    }

    public List<Accommodation> findAllAccommodations(City city, Date start_date, Date end_date){
        entityManager.getTransaction().begin();
        Query query = entityManager.createNamedQuery("find accommodation by city");
        query.setMaxResults(5);

        query.setParameter("city", city);
        query.setParameter("start_date", start_date);
        query.setParameter("end_date", end_date);
        return query.getResultList();
    }

    public void addAccommodation(Accommodation accommodation) {

        entityManager.getTransaction().begin();
        entityManager.persist(accommodation);
        entityManager.getTransaction().commit();
        entityManager.close();
    }
}
