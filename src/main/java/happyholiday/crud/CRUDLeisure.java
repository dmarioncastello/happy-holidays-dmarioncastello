package happyholiday.crud;

import happyholiday.model.City;
import happyholiday.model.Leisure;
import happyholiday.service.JPAUtility;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.Date;
import java.util.List;

public class CRUDLeisure {

    private EntityManager entityManager;

    public CRUDLeisure() {

        this.entityManager = JPAUtility.getEntityManager();
    }

    public List<Leisure> findAllLeisure(City city, Date start_date, Date end_date) {
        entityManager.getTransaction().begin();
        Query query = entityManager.createNamedQuery("find by city");
        query.setMaxResults(5);

        query.setParameter("city", city);
        query.setParameter("start_date", start_date);
        //query.setParameter("end_date", end_date);
        List<Leisure> leisures = query.getResultList();
        entityManager.close();
        return leisures;

    }
}
