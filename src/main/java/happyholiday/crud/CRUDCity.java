package happyholiday.crud;

import happyholiday.model.City;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

public class CRUDCity {

    private EntityManager entityManager;


    public CRUDCity(EntityManager entityManager){

        this.entityManager = entityManager;
    }

    public List<City> findAll(){
        entityManager.getTransaction().begin();
        List<City> cities = new ArrayList<>();
         cities = this.entityManager.createQuery("SELECT c FROM City c", City.class).getResultList();
         return cities;


    }


    public City findCity(Integer id) {
        entityManager.getTransaction().begin();
        return this.entityManager.find(City.class,id);
    }

    public void createCity(City city){
        entityManager.getTransaction().begin();
        entityManager.persist(city);
        entityManager.getTransaction().commit();
    }

    public void updateCity(Integer id, City city) {
        entityManager.getTransaction().begin();
        city.setId(id);
        entityManager.merge(city);
        entityManager.getTransaction().commit();
        entityManager.close();
    }
}
