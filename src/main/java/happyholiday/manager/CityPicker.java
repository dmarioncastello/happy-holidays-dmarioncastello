package happyholiday.manager;

import happyholiday.crud.CRUDCity;
import happyholiday.formater.FormatDestination;
import happyholiday.model.City;
import javax.persistence.EntityManager;
import java.util.List;

public class CityPicker {



    public CityPicker(){

    }

    public List<City> getCities(EntityManager entityManager){
        CRUDCity crudCity = new CRUDCity(entityManager);
        return crudCity.findAll();

    }

    public City getValidCityFromURI(EntityManager entityManager,String city){
        City newCity = new City();
        FormatDestination formatDestination = new FormatDestination();
        CRUDCity crudCity = new CRUDCity(entityManager);
        List<City> cities = formatDestination.formatCitiesToLowerCase(crudCity.findAll());
        for (City city1 : cities) {
            if(city1.getName().equals(city)){
                newCity = city1;
            }
        }
        return newCity;
    }
}
