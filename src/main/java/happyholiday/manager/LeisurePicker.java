package happyholiday.manager;

import happyholiday.crud.CRUDLeisure;
import happyholiday.formater.FormatDates;
import happyholiday.model.City;
import happyholiday.model.Leisure;

import java.util.Date;
import java.util.List;

public class LeisurePicker {

    private FormatDates formatDates;
    private City city;

    public LeisurePicker(City city){
        this.city = city;
    }

    public List<Leisure> getLeisure(String startDateTxt, String endDateTxt){
        CRUDLeisure crudLeisure = new CRUDLeisure();
        formatDates = new FormatDates();
        Date start_date = formatDates.transformDateStringIntoDate(startDateTxt);
        Date end_date = formatDates.transformDateStringIntoDate(endDateTxt);
        return crudLeisure.findAllLeisure(this.city,start_date,end_date);
    }
}
