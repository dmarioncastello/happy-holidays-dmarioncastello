package happyholiday.manager;

import happyholiday.crud.CRUDAccommodation;
import happyholiday.formater.FormatDates;
import happyholiday.model.Accommodation;
import happyholiday.model.City;

import java.util.Date;
import java.util.List;

public class AccommodationPicker {

    private FormatDates formatDates;
    private City city;

    public AccommodationPicker(City city){
        this.city = city;
    }

    public List<Accommodation> getAccomdations(String startDateTxt, String endDateTxt){
        CRUDAccommodation crudAccommodation = new CRUDAccommodation();
        formatDates = new FormatDates();
        Date start_date = formatDates.transformDateStringIntoDate(startDateTxt);
        Date end_date = formatDates.transformDateStringIntoDate(endDateTxt);
        return crudAccommodation.findAllAccommodations(this.city,start_date,end_date);
    }


}
