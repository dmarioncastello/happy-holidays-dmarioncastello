package happyholiday.manager;

import happyholiday.crud.CRUDRestaurant;
import happyholiday.model.City;
import happyholiday.model.Restaurant;

import java.util.List;

public class RestaurantPicker {

    private City city;

    public RestaurantPicker(City city){
        this.city = city;
    }

    public List<Restaurant> getRestaurants(){
        CRUDRestaurant crudRestaurant = new CRUDRestaurant();
        return crudRestaurant.findAllRestaurant(this.city);
    }

}
