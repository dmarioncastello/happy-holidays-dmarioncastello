package happyholiday.manager;

import happyholiday.model.*;
import happyholiday.service.JPAUtility;
import happyholiday.validator.ProductValidatorService;
import happyholiday.validator.ValidatorService;

import javax.persistence.EntityManager;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import java.util.List;

public class SuggeratorService {



    private City city;


    public SuggeratorService(){

    }
    public Suggestion getAllSuggestions(String product, String destination, String startDate, String endDate) {

        validateAllParameters(product, destination, startDate, endDate);
        return retrieveSuggestions(product,destination,startDate, endDate);


    }

    private void validateAllParameters(String product, String destination, String startDate, String endDate) {
        ValidatorService validatorService = new ValidatorService(product, destination, startDate, endDate);
        validatorService.validateURI();
    }

    public Boolean setProductType(String product){
        if(product.equals(ProductValidatorService.PRODUCT_TYPE_FLIGHT)){
            return true;

        }else if(product.equals(ProductValidatorService.PRODUCT_TYPE_ACCOMMODATION)){
            return false;
        }else{
            return true;
        }
    }

    public void getValidatedCity(String destination){
        CityPicker cityPicker = new CityPicker();
        EntityManager entityManager = JPAUtility.getEntityManager();
        this.city = cityPicker.getValidCityFromURI(entityManager,destination);
    }
    private List<Accommodation> suggestAccommodationByCityId(String startDateTxt, String endDateTxt){
        AccommodationPicker accommodationPicker = new AccommodationPicker(this.city);
        List<Accommodation> accommodationList = accommodationPicker.getAccomdations(startDateTxt, endDateTxt);
        return accommodationList;
    }

    private List<Leisure> suggestLeisureByCityId(String startDateTxt, String endDateTxt){
        LeisurePicker leisurePicker = new LeisurePicker(this.city);
        List<Leisure> leisures = leisurePicker.getLeisure(startDateTxt, endDateTxt);
        return leisures;
    }


    private List<Restaurant> suggestRestaurantByCityId(){
        RestaurantPicker restaurantPicker = new RestaurantPicker(this.city);
        List<Restaurant> restaurants = restaurantPicker.getRestaurants();
        return restaurants;
    }

    public Suggestion retrieveSuggestions(String product,String destination, String startDateTxt, String endDateTxt){
        getValidatedCity(destination);
        Boolean isSuggestionFlight = setProductType(product);
        List<Leisure> leisureList = suggestLeisureByCityId(startDateTxt,endDateTxt);
        List<Restaurant> restaurantList = suggestRestaurantByCityId();
        if(isSuggestionFlight){
            List<Accommodation> accommodationList = suggestAccommodationByCityId(startDateTxt, endDateTxt);
            checkCityEvents(accommodationList, leisureList, restaurantList);
            return new Suggestion(accommodationList, leisureList, restaurantList);
        }else {
            checkCityEvents(leisureList, restaurantList);
            return new Suggestion(leisureList, restaurantList);
        }

    }


    private void checkCityEvents(List<Accommodation> accommodations, List<Leisure> leisure, List<Restaurant> restaurants){
        if(accommodations.isEmpty() || leisure.isEmpty() || restaurants.isEmpty()){
            WebApplicationException webApplicationException = new WebApplicationException("City does not have events for the dates " + Response.status(422).build());
            System.out.println(webApplicationException.getMessage());
            throw new WebApplicationException(Response.status(204).build());
        }

    }

    private void checkCityEvents(List<Leisure> leisure, List<Restaurant> restaurants){
        if(leisure.isEmpty() || restaurants.isEmpty()){
            WebApplicationException webApplicationException = new WebApplicationException("City does not have events for the dates " + Response.status(422).build());
            System.out.println(webApplicationException.getMessage());
            throw new WebApplicationException(Response.status(204).build());
        }
    }



}
