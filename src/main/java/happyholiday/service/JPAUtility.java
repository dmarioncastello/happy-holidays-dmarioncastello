package happyholiday.service;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class JPAUtility {

    private static final EntityManagerFactory emFactory;

    public JPAUtility(){

    }
    static {
        emFactory = Persistence.createEntityManagerFactory("happy-holidays");
    }

    public static EntityManager getEntityManager() {
        return emFactory.createEntityManager();
    }

    public static void close() {
        emFactory.close();
    }
}
