package happyholiday.resources;


import happyholiday.crud.CRUDAccommodation;
import happyholiday.crud.CRUDCity;
import happyholiday.model.Accommodation;
import happyholiday.model.City;
import happyholiday.service.JPAUtility;

import javax.persistence.EntityManager;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;

@Path("suggestions")
public class AccommodationResources {


    @POST
    @Path("accommodations")
    @Produces("application/json")
    public Response addAccommodation(Accommodation accommodation){
        CRUDAccommodation crudAccommodation = new CRUDAccommodation();
        crudAccommodation.addAccommodation(accommodation);
        return Response.ok().build();

    }

//    @PUT
//    @Path("cities/{id}")
//    @Produces("application/json")
//    public Response updateCity(@PathParam("id") Integer id, City city){
//        CRUDCity crudCity = new CRUDCity(entityManager);
//        crudCity.updateCity(id,city);
//        return Response.ok().build();
//
//    }



}
