package happyholiday.resources;

import happyholiday.manager.SuggeratorService;
import happyholiday.model.Suggestion;
import happyholiday.validator.ValidatorService;
import javax.ws.rs.*;

@Path("suggestions")
public class SuggestionResources {

    @GET
    @Path("{product}/{destination}")
    @Produces("application/json")
    public Suggestion getJSONArchytecture(@PathParam("product") String product, @PathParam("destination") String destination,
                                                @QueryParam("start-date") String startDate, @QueryParam("end-date") String endDate) throws WebApplicationException{

        SuggeratorService suggeratorService = new SuggeratorService();
        return suggeratorService.getAllSuggestions(product, destination, startDate, endDate);


    }




}
