package happyholiday.resources;

import happyholiday.crud.CRUDCity;
import happyholiday.model.City;
import happyholiday.service.JPAUtility;

import javax.persistence.EntityManager;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("suggestions")
public class AddCitiesResources {

    private EntityManager entityManager = JPAUtility.getEntityManager();


    @GET
    @Path("cities")
    @Produces("application/json")
    public List<City> getCities(){
        EntityManager entityManager = JPAUtility.getEntityManager();
        CRUDCity crudCity = new CRUDCity(entityManager);
        return crudCity.findAll();


    }

    @GET
    @Path("cities/{id}")
    @Produces("application/json")
    public City getCityById(@PathParam("id") Integer id){
        EntityManager entityManager = JPAUtility.getEntityManager();
        CRUDCity crudCity = new CRUDCity(entityManager);
        return crudCity.findCity(id);

    }

    @POST
    @Path("cities")
    @Produces("application/json")
    public Response addCity(City city){
        CRUDCity crudCity = new CRUDCity(entityManager);
        crudCity.createCity(city);
        return Response.ok().build();

    }

    @PUT
    @Path("cities/{id}")
    @Produces("application/json")
    public Response updateCity(@PathParam("id") Integer id, City city){
        CRUDCity crudCity = new CRUDCity(entityManager);
        crudCity.updateCity(id,city);
        return Response.ok().build();

    }



}
