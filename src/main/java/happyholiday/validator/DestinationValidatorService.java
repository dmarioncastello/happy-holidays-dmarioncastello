package happyholiday.validator;

import happyholiday.formater.FormatDestination;
import happyholiday.model.City;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import java.util.List;

public class DestinationValidatorService implements Validator {

    private List<City> cities;
    private String destination;

    public DestinationValidatorService(String destination, List<City> cities) {
        this.destination = destination;
        this.cities = cities;
    }

    @Override
    public Response handleParameter() {
        FormatDestination formatDestination = new FormatDestination();
        List<City> cities = formatDestination.formatCitiesToLowerCase(this.cities);
        Boolean validValues = checkDestinationExists(cities);
        if (validValues == false) {
          return exceptionMessage("Invalid Path parameter for destination, must be " +
                  formatDestination.buildCityNames(this.cities) + " ");

        } else {
            return Response.ok().build();
        }
    }

    public Boolean checkDestinationExists(List<City> cities) {
        Boolean validProduct = true;
        if (!cities.stream().anyMatch(p -> p.getName().equals(this.destination))) {
            validProduct = false;
        }
        return validProduct;
    }
    public Response exceptionMessage(String exceptionMessage){
        WebApplicationException webApplicationException = new WebApplicationException("" +exceptionMessage
                 + Response.status(422).build());
        System.out.println(webApplicationException.getMessage());
        throw new WebApplicationException(Response.status(422).build());
    }



}
