package happyholiday.validator;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

public class ProductValidatorService implements Validator{

    public static final String PRODUCT_TYPE_ACCOMMODATION = "accommodation";
    public static final String PRODUCT_TYPE_FLIGHT = "flight";
    private String product;

    public ProductValidatorService(String product){
        this.product = product;
    }

    @Override
    public Response handleParameter() {
        Boolean validValues =  checkProductExists();
        if(validValues == false){
            return exceptionMessage("Invalid paramter must be" + PRODUCT_TYPE_ACCOMMODATION + " or" + PRODUCT_TYPE_FLIGHT);

        }else {

            return Response.ok().build();
        }
    }

    public Boolean checkProductExists()  {

        Boolean validProduct = true;
        if(!this.product.equals(PRODUCT_TYPE_ACCOMMODATION) && !this.product.equals(PRODUCT_TYPE_FLIGHT)){
            validProduct = false;
        }
        return validProduct;
    }

    public Response exceptionMessage(String exceptionMessage){
        WebApplicationException webApplicationException = new WebApplicationException(exceptionMessage + Response.status(422).build());
        System.out.println(webApplicationException.getMessage());
        throw new WebApplicationException(Response.status(422).build());
    }
}
