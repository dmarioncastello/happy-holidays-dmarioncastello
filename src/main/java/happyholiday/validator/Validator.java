package happyholiday.validator;

import javax.ws.rs.core.Response;

public interface Validator {

     Response handleParameter();

     Response exceptionMessage(String exceptionMessage);


}
