package happyholiday.validator;

import happyholiday.formater.FormatDates;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateValidatorService implements Validator{

    private String startDateTxt;
    private String endDateTxt;

    public DateValidatorService(String startDateTxt, String endDateTxt){
        this.startDateTxt = startDateTxt;
        this.endDateTxt = endDateTxt;
    }
    public DateValidatorService(){

    }


    @Override
    public Response handleParameter() {
        FormatDates formatDates = new FormatDates();
        Date startDate = formatDates.transformDateStringIntoDate(this.startDateTxt);
        Date endDate = formatDates.transformDateStringIntoDate(this.endDateTxt);
        if(checkStartDateBeforeThanEndDate(startDate, endDate) == false){
            return exceptionMessage("Invalid start date parameter, start date must be smaller than end date");
        }else if(checkStartDateAfterActualDate(startDate) == false){
           return  exceptionMessage("Invalid end date parameter, booking start date must be bigger than local date");

        }else{
                return Response.ok().build();
            }
        }



    public Boolean checkStartDateBeforeThanEndDate(Date startDate, Date endDate){
        Boolean startDateIsSmallerThanEndDate = true;
        if(startDate.after(endDate)){
            startDateIsSmallerThanEndDate = false;
        }
        return startDateIsSmallerThanEndDate;
    }

    public Boolean checkStartDateAfterActualDate(Date startDate){
        FormatDates formatDates = new FormatDates();
        Date currentDate = formatDates.getLocalDate();
        Boolean startDateIsAfterActualDate = true;
        if(startDate.before(currentDate)){
        startDateIsAfterActualDate = false;
        }
        return startDateIsAfterActualDate;
    }

    public Response exceptionMessage(String exceptionMessage){
        WebApplicationException webApplicationException = new WebApplicationException(exceptionMessage + Response.status(422).build());
        System.out.println(webApplicationException.getMessage());
        throw new WebApplicationException(Response.status(422).build());
    }
}
