package happyholiday.validator;

import happyholiday.manager.CityPicker;
import happyholiday.service.JPAUtility;

import javax.persistence.EntityManager;
import javax.ws.rs.core.Response;

public class ValidatorService {

    private Validator productValidator;
    private Validator destinationValidator;
    private Validator datesValidator;
    private EntityManager entityManager;

    private String product;
    private String destination;
    private String startDate;
    private String endDate;

    public ValidatorService(String product, String destination, String startDate, String endDate){
        this.product = product;
        this.destination = destination;
        this.startDate = startDate;
        this.endDate = endDate;
        this.entityManager = JPAUtility.getEntityManager();
    }

    public Response validateURI(){

        validateProduct();
        validateDestination();
        validateDates();
        return Response.ok().build();
    }

    private void validateDestination(){

        CityPicker cityPicker = new CityPicker();
        destinationValidator = new DestinationValidatorService(destination,cityPicker.getCities(entityManager));
        destinationValidator.handleParameter();
        datesValidator = new DateValidatorService(this.startDate, this.endDate);
        datesValidator.handleParameter();
    }

    private void validateProduct(){
        productValidator = new ProductValidatorService(product);
        productValidator.handleParameter();
    }

    private void validateDates(){
        datesValidator = new DateValidatorService(this.startDate, this.endDate);
        datesValidator.handleParameter();
    }



}
