package happyholiday.model;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import javax.persistence.*;
import java.util.Date;

@Entity
@NamedQuery(query = "Select r from Restaurant r where r.city = :city ORDER BY r.sponsored DESC, r.rating DESC"
                    , name = "find restaurant by city")
@JsonPropertyOrder(value = {"id", "name", "address","enabled", "sponsored", "rating", "city"})
public class Restaurant {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private String address;
    private Boolean enabled;

    private String name;
    private Boolean sponsored;
    private Double rating;

    @ManyToOne
    City city;


    public Restaurant(){


    }
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }



    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getSponsored() {
        return sponsored;
    }

    public void setSponsored(Boolean sponsored) {
        this.sponsored = sponsored;
    }

    public Double getRating() {
        return rating;
    }

    public void setRating(Double rating) {
        this.rating = rating;
    }

    @Override
    public String toString() {
        return "Restaurant{" +
                "id=" + id +
                ", address='" + address + '\'' +
                ", enabled=" + enabled +
                ", name='" + name + '\'' +
                ", sponsored=" + sponsored +
                ", rating=" + rating +
                '}';
    }
}


