package happyholiday.model;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.util.List;

@Entity
public class City {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private String name;
    private String country;


    @JsonBackReference
    @OneToMany(mappedBy = "city")
    private List<Accommodation> accommodations;

    @JsonBackReference
    @OneToMany(mappedBy = "city")
    private List<Leisure> leisures;

    public City(){

    }
    public City(String name, String country){
        this.name = name;
        this.country = country;
    }

    public City(Integer id, String name, String country) {
        this.id = id;
        this.name = name;
        this.country = country;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

//    public List<Accommodation> getAccommodations() {
//        return accommodations;
//    }
//
//    public void setAccommodations(List<Accommodation> accommodations) {
//        this.accommodations = accommodations;
//    }

    @Override
    public String toString() {
        return "City{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", country='" + country + '\'' +
                '}';
    }
}
