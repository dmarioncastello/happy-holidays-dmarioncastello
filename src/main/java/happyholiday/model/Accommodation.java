package happyholiday.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table
@NamedQuery(query = "Select a from Accommodation a where a.city = :city AND a.end_date > :start_date AND a.start_date < :end_date " +
                     "ORDER BY a.sponsored DESC, " +
                     "a.rating DESC", name = "find accommodation by city")
@JsonPropertyOrder(value = {"id", "name", "address","enabled", "sponsored", "rating", "start_date", "end_date","city"})
public class Accommodation {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private String address;
    private Boolean enabled;
    @JsonFormat(pattern="yyyy-MM-dd")
    private Date start_date;

    @JsonFormat(pattern="yyyy-MM-dd")
    private Date end_date;

    private String name;
    private Boolean sponsored;
    private Double rating;

    @ManyToOne
    City city;


    public Accommodation(){


    }

    public Accommodation(String address, Boolean enabled, Date start_date, Date end_date, String name, Boolean sponsored, Double rating) {
        this.address = address;
        this.enabled = enabled;
        this.start_date = start_date;
        this.end_date = end_date;
        this.name = name;
        this.sponsored = sponsored;
        this.rating = rating;
    }

    public Integer getId() {
        return id;
    }
    public String getName() {
        return name;
    }
    public String getAddress() {
        return address;
    }
    public Boolean getEnabled() {
        return enabled;
    }
    public Boolean getSponsored() {
        return sponsored;
    }
    public Date getStart_date() {
        return start_date;
    }
    public Date getEnd_date() {
        return end_date;
    }


    public void setAddress(String address) {
        this.address = address;
    }



    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public void setStart_date(Date start_date) {
        this.start_date = start_date;
    }

    public void setEnd_date(Date end_date) {
        this.end_date = end_date;
    }

    public void setId(Integer id) {
        this.id = id;
    }



    public void setName(String name) {
        this.name = name;
    }


    public void setSponsored(Boolean sponsored) {
        this.sponsored = sponsored;
    }

    public Double getRating() {
        return rating;
    }

    public void setRating(Double rating) {
        this.rating = rating;
    }

    @Override
    public String toString() {
        return "Accommodation{" +
                "id=" + id +
                ", address='" + address + '\'' +
                ", enabled=" + enabled +
                ", start_date=" + start_date +
                ", end_date=" + end_date +
                ", name='" + name + '\'' +
                ", sponsored=" + sponsored +
                ", rating=" + rating +
                '}';
    }
}
