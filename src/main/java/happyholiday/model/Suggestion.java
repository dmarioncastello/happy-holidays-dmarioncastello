package happyholiday.model;

import java.util.List;

public class Suggestion {

    private List<Accommodation> accommodationList;
    private List<Leisure> leisureList;
    private List<Restaurant> restaurantList;

    public Suggestion(List<Accommodation> accommodations, List<Leisure> leisure, List<Restaurant> restaurants){

        this.accommodationList = accommodations;
        this.leisureList = leisure;
        this.restaurantList = restaurants;

    }
    public Suggestion(List<Leisure> leisure, List<Restaurant> restaurants) {

        this.restaurantList = restaurants;
        this.leisureList = leisure;
    }

    public List<Accommodation> getAccommodationList() {
        return accommodationList;
    }

    public List<Leisure> getLeisureList() {
        return leisureList;
    }

    public List<Restaurant> getRestaurantList() {
        return restaurantList;
    }
}
