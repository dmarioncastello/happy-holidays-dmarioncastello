package happyholiday.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table
@NamedQuery(query = "Select l from Leisure l where l.city = :city AND :start_date BETWEEN l.start_date AND l.end_date " +
                     "ORDER BY l.sponsored DESC, l.rating DESC", name = "find by city")
@JsonPropertyOrder(value = {"id", "name", "address","enabled", "sponsored", "rating", "start_date", "end_date","city"})
public class Leisure {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private String name;
    private String address;

    @JsonFormat(pattern="yyyy-MM-dd")
    private Date start_date;

    @JsonFormat(pattern="yyyy-MM-dd")
    private Date end_date;

    private Boolean enabled;
    private Boolean sponsored;
    private Double rating;

    @ManyToOne
    City city;

    public Leisure(){

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public Boolean getSponsored() {
        return sponsored;
    }

    public void setSponsored(Boolean sponsored) {
        this.sponsored = sponsored;
    }

    public Double getRating() {
        return rating;
    }

    public void setRating(Double rating) {
        this.rating = rating;
    }

    public Date getStart_date() {
        return start_date;
    }

    public Date getEnd_date() {
        return end_date;
    }

    public void setStart_date(Date start_date) {
        this.start_date = start_date;
    }

    public void setEnd_date(Date end_date) {
        this.end_date = end_date;
    }

    @Override
    public String toString() {
        return "Leisure{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", start_date=" + start_date +
                ", end_date=" + end_date +
                ", enabled=" + enabled +
                ", sponsored=" + sponsored +
                ", rating=" + rating +
                '}';
    }
}
